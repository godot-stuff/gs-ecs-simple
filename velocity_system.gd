class_name VelocitySystem
extends System


func on_process_entity(entity : Entity, delta: float):
	var _component = entity.get_component("velocity") as Velocity
	entity.position += _component.direction * _component.speed * delta
